FROM node as builder-node
ENV SERIES 7.0
RUN npm install -g bower
RUN curl -k https://downloads.tryton.org/${SERIES}/tryton-sao-last.tgz | tar zxf - -C /
RUN cd /package && bower install --allow-root

FROM python:3.12
LABEL maintainer="Datalife <info@datalifeit.es>" \
    org.label-schema.name="Tryton" \
    org.label-schema.url="http://www.datalifeit.es/" \
    org.label-schema.vendor="Datalife" \
    org.label-schema.version="7.0" \
    org.label-schema.schema-version="1.0"

ENV SERIES 7.0
ENV LANG C.UTF-8
ENV USER="trytond"
ENV HOME_DIR="/home/${USER}"
ENV WORK_DIR="${HOME_DIR}/tryton"

RUN groupadd -r trytond \
    && useradd --no-log-init -r -m -g trytond trytond \
    && mkdir /home/trytond/tryton && chown trytond:trytond /home/trytond/tryton \
    && mkdir /home/trytond/db && chown trytond:trytond /home/trytond/db \
    && mkdir /home/trytond/www \
    && mkdir -p /etc/python3 \
    && echo "[DEFAULT]\nbyte-compile = standard, optimize" \
        > /etc/python3/debian_config

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        curl \
        mercurial \
        git \
        vim \
        gunicorn \
        python3-argcomplete \
        # trytond
        python3-argon2 \
        python3-bcrypt \
        python3-cffi \
        python3-genshi \
        python3-gevent \
        python3-html2text \
        python3-pil \
        python3-levenshtein \
        python3-lxml \
        python3-passlib \
        python3-polib \
        python3-psycopg2 \
        python3-pydot \
        python3-werkzeug \
        python3-wrapt \
        # modules
        python3-dateutil \
        python3-ldap3 \
        python3-magic \
        python3-ofxparse \
        python3-pypdf \
        python3-pysimplesoap \
        python3-requests \
        python3-simpleeval \
        python3-tz \
        python3-yaml \
        python3-zeep \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /etc/bash_completion.d \
    && activate-global-python-argcomplete

# Add local content to Docker
USER trytond
ENV PATH="${HOME_DIR}/.local/bin:${PATH}"
COPY --chown=trytond:trytond content ${WORK_DIR}
COPY --chown=trytond:trytond ./.hgrc ${HOME_DIR}
COPY --chown=trytond:trytond ./.hgignore ${HOME_DIR}

# Configure git reconcile to avoid message on pull
RUN git config --global pull.rebase false

# Configure vim
RUN echo 'set number' > ~/.virc

WORKDIR ${WORK_DIR}

RUN pip3 install --break-system-packages --no-cache-dir --user \
    "proteus == ${SERIES}.*" \
    && pip3 install --break-system-packages --no-cache-dir --user \
        python-barcode[images] \
        qrcode[pil] \
        webcolors \
        pycountry \
        pygal \
        python-stdnum[SOAP] \
        schwifty \
        'psycopg2 >= 2.5' \
    && pip3 install --no-cache-dir --user \
        git+https://gitlab.com/datalifeit/tryton-dvconfig.git \
    && python3 -c "import compileall; compileall.compile_path(maxlevels=10, optimize=1)"

USER root
COPY --from=builder-node /package /home/trytond/www
COPY entrypoint.sh /

EXPOSE 8000

VOLUME ["/home/trytond/db", "/home/trytond/tryton"]
ENV TRYTOND_CONFIG=${WORK_DIR}/etc/trytond.conf
USER trytond
COPY gunicorn.conf.py ${WORK_DIR}/etc/gunicorn.conf.py
ENTRYPOINT ["/entrypoint.sh"]
CMD ["gunicorn", "--config", "/home/trytond/tryton/etc/gunicorn.conf.py"]

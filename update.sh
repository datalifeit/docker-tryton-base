#!/bin/sh

docker login registry.gitlab.datalife.es

docker build --rm --no-cache -t registry.gitlab.datalife.es/datalife/docker-tryton-base:7.0 .
docker build --rm --no-cache -t registry.gitlab.datalife.es/datalife/docker-tryton-base:7.0-office office

docker push registry.gitlab.datalife.es/datalife/docker-tryton-base:7.0-office
docker push registry.gitlab.datalife.es/datalife/docker-tryton-base:7.0
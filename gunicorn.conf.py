import os

wsgi_app = 'trytond.application:app'
bind = '0.0.0.0:8000'
raw_env = [
    'TRYTOND_CONFIG=%s' % os.environ.get('TRYTOND_CONFIG', ''),
    'TRYTOND_DATABASE_URI=%s' % os.environ.get('TRYTOND_DATABASE_URI', ''),
    'PYTHONOPTIMIZE=%s' % os.environ.get('PYTHONOPTIMIZE'),
    'TRYTOND_LOGGING_LEVEL=%s' % os.environ.get('TRYTOND_LOGGING_LEVEL', 40)
    ]
timeout = 300
#workers = 1
#threads = 4
